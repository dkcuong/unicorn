package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CenturyProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(CenturyProjectApplication.class, args);
	}

}
